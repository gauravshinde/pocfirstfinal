const Validator = require('validator');
const isEmpty = require('./is-empty');

module.exports = function validateLoginInput(data){
  let errors = {};

  
  data.username = !isEmpty(data.username) ? data.username: '';
  data.password = !isEmpty(data.password) ? data.password: '';
  


// Username
if(Validator.isEmpty(data.username)){
  errors.username = 'Username field isUsernamed';
}

//passord

  if(!Validator.isLength(data.password, { min:6, max:30 })){
    errors.password = 'password must be between 6 and 30 characters';
  }

  if(Validator.isEmpty(data.password)){
    errors.password = 'Password field is required';
  }


  return{
    errors,
    isValid: isEmpty(errors)
  }
}