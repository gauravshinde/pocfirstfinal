import React from "react";
// import Dropdown from "react-dropdown";
// import "react-dropdown/style.css";

// const options = ["Not Seated", "Seated", "Completed", "Cancelled"];
// const defaultOption = options[0];
import PopUp from "../SmallComponents/PopUp";
import EditWaitTime from "./../VendorDashboard/EditWaitTime";
import { OccassionPopup } from "../SmallComponents/OccassionPopup";

const VendorTableHeadSubRow = ({
  dinner,
  dinnerID,
  no,
  Name,
  Phone,
  sendphonenumber,
  // OrderType,
  Adults,
  Kids,
  Total,
  Occassion,
  WaitTime,
  Timer,
  Status,
  Img,
  Img1,
  Img2,
  Alt,
  onClickone,
  inputWaitTime,
  onWaitTimeChange,
  onClicktwo,
  onSelectStatusChange,
  timerKey,
  onWaitTimeSubmit,
  seatingStatus
}) => {
  console.log(sendphonenumber);
  return (
    <React.Fragment>
      <tr className="second-row">
        <td>{no}</td>
        <td>{Name}</td>
        <td className="text-left">
          {Phone}
          <PopUp imgsrc={Img1} dinner={sendphonenumber} />
          {/* dinner={dinner} */}
        </td>
        <td>
          <img src={Img} alt={Alt} />
        </td>
        <td>{Adults}</td>
        <td>{Kids}</td>
        <td>{Total}</td>
        <td>
          {/* <OccassionPopup dinner={Occassion} /> */}
          {Occassion}
        </td>
        <td>
          <EditWaitTime dinner={dinner} />
          {/* {WaitTime} */}
          {/* {(dinner = { dinner })} */}
        </td>
        <td id={timerKey}>{Timer}</td>
        <td>
          <div className="form-group mb-0">
            {/* <Dropdown options={options} onChange={onChange} value={Status} /> */}
            <select
              onChange={onSelectStatusChange(dinnerID)}
              className="form-control p-0 size-select"
            >
              {Status.map((option, index) => (
                <option
                  key={index}
                  value={option}
                  selected={seatingStatus === option ? "selected" : ""}
                >
                  {option}
                </option>
              ))}
            </select>
          </div>
        </td>
      </tr>
    </React.Fragment>
  );
};

export default VendorTableHeadSubRow;
