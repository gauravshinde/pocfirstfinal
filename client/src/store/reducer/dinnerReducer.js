import { DINNER_LIST } from "../types";

const initialState = {
  dinner: []
};

export default function(state = initialState, action) {
  switch (action.type) {
    case DINNER_LIST:
      return {
        dinner: action.payload
      };
    default:
      return state;
  }
}
