import axios from "axios";

import { VENDOR_DETAILS } from "../types";

//get vendor data

export const getVendordetails = () => dispatch => {
  axios
    .get("/all_vendor/:username")
    .then(res =>
      dispatch({
        type: VENDOR_DETAILS,
        payload: res.data
      })
    )
    .catch(err => console.log(err));
};
