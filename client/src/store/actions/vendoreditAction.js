import axios from "axios";

import { VENDOR_EDIT } from "../types";

export const getVendoredit = (formData, history) => dispatch => {
  console.log(formData);
  axios
    .patch(`/vendor/vendor-update`, formData)
    // .patch("/all_vendor/:vendorId", editUser)
    .then(res => history.push("/vendor-list"))
    .catch(err =>
      dispatch({
        type: VENDOR_EDIT,
        payload: err.response.data
      })
    );
};
