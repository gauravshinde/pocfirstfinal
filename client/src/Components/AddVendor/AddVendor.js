import React, { Component } from "react";
import HeaderComponent from "../Header/HeaderComponent";
import LargeText from "../SmallComponents/LargeText";
import InputComponent from "../SmallComponents/InputComponent";
import ButtonComponent from "../SmallComponents/ButtonComponent";
import { Link, withRouter } from "react-router-dom";
import TextBoxComponent from "../SmallComponents/TextBoxComponent";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { registerUser } from "../../store/actions/authAction";
import { logoutUser } from "../../store/actions/authAction";
// import TextBoxComponent from '../SmallComponents/TextBoxComponent';

export class AddVendor extends Component {
  constructor() {
    super();
    this.state = {
      formOne: true,
      formTwo: false,
      vendor_name: "",
      email: "",
      phone_number: "",
      address: "",
      username: "",
      password: "",
      password2: "",
      errors: {}
    };
  }

  componentDidMount() {
    // If logged in and user navigates to Register page, should redirect them to dashboard
    if (this.props.auth.isAuthenticated) {
      this.props.history.push("/add-vendor");
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.errors) {
      this.setState({
        errors: nextProps.errors
      });
    }
  }

  cancelClickHandler = e => {
    this.setState({
      formOne: true,
      formTwo: false
    });
  };

  nextClickHandler = e => {
    if (this.state.vendor_name === "") {
      window.alert("Fill form");
    } else {
      this.setState({
        formOne: false,
        formTwo: true
      });
    }
  };

  handleAddVendorChange = e => {
    this.setState({
      [e.target.name]: e.target.value,
      error: {}
    });
  };

  handleAddVendorSubmit = e => {
    e.preventDefault();
    const { password, password2 } = this.state;
    // perform all neccassary validations
    if (password !== password2) {
      window.alert("Passwords don't match");
    } else {
      const newUser = {
        vendor_name: this.state.vendor_name,
        email: this.state.email,
        phone_number: this.state.phone_number,
        address: this.state.address,
        username: this.state.username,
        password: this.state.password,
        password2: this.state.password2
      };

      this.props.registerUser(newUser, this.props.history);
    }
  };

  onLogoutClick = e => {
    e.preventDefault();
    this.props.logoutUser(this.props.history);
  };

  render() {
    const { errors } = this.state;
    const { formOne, formTwo } = this.state;

    const form1 = (
      <div className="form1">
        <form noValidate onSubmit={this.handleAddVendorSubmit}>
          <InputComponent
            img={require("./../../assets/Images/usernamepic.svg")}
            alt={"user-img"}
            labeltext={"Vendor Name"}
            name={"vendor_name"}
            type={"text"}
            place={"eg. James Bond"}
            inputclass={"form-control input-bottomblack "}
            value={this.state.vendor_name}
            onChange={this.handleAddVendorChange}
            error={errors.vendor_name}
          />

          <InputComponent
            img={require("./../../assets/Images/email-img.svg")}
            alt={"Email-img"}
            labeltext={"Email"}
            name={"email"}
            type={"email"}
            place={"eg. something@something.com"}
            inputclass={"form-control input-bottomblack "}
            value={this.state.email}
            onChange={this.handleAddVendorChange}
            error={errors.email}
            required
          />

          <InputComponent
            img={require("./../../assets/Images/phone-img.svg")}
            alt={"Phone-img"}
            labeltext={"Phone Number"}
            name={"phone_number"}
            type={"Number"}
            place={"eg. 123567890"}
            inputclass={"form-control input-bottomblack "}
            value={this.state.phone_number}
            onChange={this.handleAddVendorChange}
            error={errors.phone_number}
            required
          />

          <TextBoxComponent
            img={require("./../../assets/Images/location-img.svg")}
            alt={"Address-img"}
            name={"address"}
            type={"text"}
            labeltext={"Address"}
            place={"eg. Riverdale"}
            Textareaclass={"form-control textarea-bottomblack "}
            value={this.state.address}
            onChange={this.handleAddVendorChange}
            error={errors.address}
            required
          />

          <ButtonComponent
            buttontype={"submit"}
            buttonclass={"login-button"}
            buttontext={"Next"}
            handleOnClick={this.nextClickHandler}
          />

          <Link to="/vendor-list">
            <ButtonComponent
              buttontype={"button"}
              buttonclass={"cancel-button"}
              buttontext={"Cancel"}
              // handleOnClick={this.cancelClickHandler}
            />
          </Link>
        </form>
      </div>
    );

    const form2 = (
      <div className="form1">
        <form noValidate onSubmit={this.handleAddVendorSubmit}>
          <InputComponent
            img={require("./../../assets/Images/usernamepic.svg")}
            alt={"user-img"}
            labeltext={"Username"}
            name={"username"}
            type={"text"}
            place={"eg. James Bond"}
            inputclass={"form-control input-bottomblack "}
            value={this.state.username}
            onChange={this.handleAddVendorChange}
            error={errors.username}
          />

          <InputComponent
            img={require("../../assets/Images/passwordpic.svg")}
            alt={"Password"}
            labeltext={"Password"}
            name={"password"}
            type={"password"}
            place={"**********"}
            inputclass={"form-control input-bottomblack "}
            value={this.state.password}
            onChange={this.handleAddVendorChange}
            error={errors.password}
          />

          <InputComponent
            img={require("../../assets/Images/passwordpic.svg")}
            alt={"confirm password"}
            labeltext={"Confirm Password"}
            name={"password2"}
            type={"password"}
            place={"**********"}
            inputclass={"form-control input-bottomblack "}
            value={this.state.password2}
            onChange={this.handleAddVendorChange}
            error={errors.password2}
          />

          <ButtonComponent
            buttontype={"submit"}
            buttonclass={"login-button"}
            buttontext={"Save"}
            handleOnClick={this.nextClickHandler}
          />

          <ButtonComponent
            buttontype={"button"}
            buttonclass={"cancel-button"}
            buttontext={"Back"}
            handleOnClick={this.cancelClickHandler}
          />
        </form>
      </div>
    );
    return (
      <React.Fragment>
        <HeaderComponent
          headertext={"Logo"}
          vendorDashboard={true}
          addVendorBtn={false}
          editVendorbtn={false}
          link={"/vendor-list"}
          onClick={this.onLogoutClick}
        />
        <div className="container text-center">
          <div className="row">
            <div className="col-md-4" />

            <div className="col-md-4 add-vendor-align">
              <LargeText
                formOne={formOne}
                largetext={"Add Vendor"}
                largetextclass={"addvendor-text"}
                addvendorform={true}
                count={true}
                mainclass={"col-12 my-3 px-0 d-flex justify-content-between"}
              />
              <hr />

              {formOne === true && formTwo === false
                ? form1
                : formOne === false && formTwo === true
                ? form2
                : null}
            </div>
            <div className="col-md-4" />
          </div>
        </div>
      </React.Fragment>
    );
  }
}

AddVendor.propTypes = {
  logoutUser: PropTypes.func.isRequired,
  registerUser: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired,
  errors: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  auth: state.auth,
  errors: state.errors
});

export default connect(
  mapStateToProps,
  { logoutUser, registerUser }
)(withRouter(AddVendor));
