const express = require("express");
const router = express.Router();
const Dinner = require("../models/add_dinner");
const default_time = require("../models/default");
const jwt = require("jsonwebtoken");
const keys = require("../config/keys");
const add_dinner_validation = require("../validation/add_dinner");
const msg91 = require("msg91")("231660ASmgnCOLK5b728a5e", "SomiDb", "4");
// const accountSid = "ACa7e8734f7243eb4e8b322481d1a4376a";
// const authToken = "9f375582bf6960e4ec9e6c6ef5e8eeba";
// const client = require("twilio")(accountSid, authToken);
const accountSid = "ACa7e8734f7243eb4e8b322481d1a4376a";
const authToken = "9f375582bf6960e4ec9e6c6ef5e8eeba";
const client = require("twilio")(accountSid, authToken);
//Sending SMS
const SMS = require("../validation/sms_sending");

// Update Wait_time with SMS (Gourav Send)
router.patch("/update_waitTime", async (req, res, next) => {
  try {
    const body = req.body;
    let updateDinnerWaitTime = await Dinner.findOneAndUpdate(
      { _id: body._id },
      {
        $set: {
          wait_time: body.wait_time
        }
      }
    );
    if (updateDinnerWaitTime) {
      const mobileNo = updateDinnerWaitTime.phone_number;
      const name = updateDinnerWaitTime.name;
      console.log(mobileNo);
      if (mobileNo) {
        console.log("Sending SMS");
        msg91.send(
          mobileNo,
          `Hi ${name}, Your current waiting time is ${body.wait_time}. minute Thank You`,
          (error, response) => {
            res.json(response);
          }
        );
      } else {
        console.log("SMS not sent");
        return res.status(200).json("Wait Time Updated But Unable to send SMS");
      }
    }
  } catch (err) {
    console.log(err);
  }
});

// Get To All the Dinner List Using /all_dinner route

router.get("/all_dinner", (req, res, next) => {
  Dinner.find()
    .exec()
    .then(DinnerList => {
      let dinnerList = DinnerList.reverse();
      res.status(200).json(dinnerList);
    })
    .catch(err => {
      res.status(500).json({
        error: err
      });
    });
});

// Insert Dinner Using /add_dinner POST method

router.post("/add_dinner", (req, res, next) => {
  const { errors, isValid } = add_dinner_validation(req.body);
  // Check validation
  if (!isValid) {
    return res.status(400).json(errors);
  }
  if (req.headers && req.headers.authorization) {
    var authorization = req.headers.authorization;
    const token = authorization.split(" ")[1];
    try {
      decoded = jwt.verify(token, keys.secretOrKey);
    } catch (e) {
      return res.status(401).send("unauthorized");
    }
    const vendorId = decoded.id;
    const dinner = new Dinner({
      vendor_id: vendorId,
      name: req.body.name,
      phone_number: req.body.phone_number,
      country_code: req.body.country_code,
      adults: req.body.adults,
      kids: req.body.kids,
      total: parseInt(req.body.adults) + parseInt(req.body.kids),
      wait_time: req.body.wait_time,
      special_occassion: req.body.special_occassion,
      status: "Nonseated"
    });
    dinner
      .save()
      .then(result => {
        client.messages.create(
          {
            body: "Your request has been confirmed...!",
            messagingServiceSid: "MG47e9a75679be5a94cf3c00e9fa1837dd",
            form: "+14152003837",
            to: "+" + dinner.phone_number
          },
          res.status(200).json({ message: "Dinner Added SuccessFully..!" })
        );
      })
      .catch(err => {
        res.status(500).json({ error: err });
      });
  }
});

//Update Status
router.patch("/status/", (req, res, next) => {
  const body = req.body;
  Dinner.findByIdAndUpdate(
    { _id: body.dinnerId },
    { $set: { status: req.body.status } }
  )
    .exec()
    .then(result => {
      if (req.body.status === "Seated") {
        client.messages.create(
          {
            body: "Bon Appetit! You have now been seated.",
            messagingServiceSid: "MG47e9a75679be5a94cf3c00e9fa1837dd",
            form: "+14152003837",
            to: result.phone_number
          },
          res.status(200).json({ message: "Status Update SuccessFully..!" })
        );
      } else if (req.body.status === "Completed") {
        client.messages.create(
          {
            body:
              "You have been completed. We hope your have enjoyed our food. Visit us again",
            messagingServiceSid: "MG47e9a75679be5a94cf3c00e9fa1837dd",
            form: "+14152003837",
            to: result.phone_number
          },
          res.status(200).json({ message: "Status Update SuccessFully..!" })
        );
      } else if (req.body.status === "Cancelled") {
        client.messages.create(
          {
            body:
              "Oops, we're sorry! Your " +
              req.body.status +
              " request is rejected. Request again for a " +
              req.body.status +
              " in here.",
            messagingServiceSid: "MG47e9a75679be5a94cf3c00e9fa1837dd",
            form: "+14152003837",
            to: result.phone_number
          },
          res.status(200).json({ message: "Status Update SuccessFully..!" })
        );
      }
    })
    .catch(err => {
      res.status(500).json({ message: "Status Not Update SuccessFully..!" });
    });
});

//send sms

// router.post('/send', (req, res) => {
//   const body = req.body;
//   //console.log(body);
//   if (!body.mobileNo || !body.message) {
//     return res.status(400).json('Error, Data to besent is invalid');
//   } else {
//     msg91.send(body.mobileNo, body.message, (err, response) => {
//       res.json(response);
//     });
//   }
// });

router.post("/send_sms", (req, res, next) => {
  const body = req.body;
  client.messages.create({
    body: body.message,
    messagingServiceSid: "MG47e9a75679be5a94cf3c00e9fa1837dd",
    form: "+14152003837",
    to: body.mobileNo
  });
});

//Update Default Wait Time
// router.patch('/wait_time', (req, res, next) => {
//     const body = req.body;
//     const default_wait_time = 0;
//     Dinner.findOne({ _id: body.id })
//         .exec()
//         .then(DinnerList => {
//             if (body.wait_time == null) {
//                 Dinner.findByIdAndUpdate({ _id: body.id }, { $set: { wait_time: default_wait_time } })
//                     .exec()
//                     .then(result => {
//                         res.status(200).json({ message: 'Wait Time Update SuccessFully..!' });
//                     })
//                     .catch(err => {
//                         res.status(500).json({ message: 'Wait Time Not Update SuccessFully..!' });
//                     });
//             }
//             else {
//                 Dinner.findByIdAndUpdate({ _id: body.id }, { $set: { wait_time: req.body.wait_time } })
//                     .exec()
//                     .then(result => {
//                         res.status(200).json({ message: 'Wait Time Update SuccessFully..!' });
//                     })
//                     .catch(err => {
//                         res.status(500).json({ message: 'Wait Time Not Update SuccessFully..!' });
//                     });

//             }
//         });
// });

//  Geting Waiting Time

router.get("/get_wait_time", async (req, res, next) => {
  try {
    let defaultWaitTime = await default_time.findOne(
      { _id: "5d41bea940aa480edade59b3" },
      { default_time: 1, _id: 0 }
    );
    return res.status(200).json(defaultWaitTime);
  } catch (err) {
    return res.status(400).json("Error");
  }
});

// Update Wait_time with SMS (Gourav Send)
// router.patch("/update_waitTime", async (req, res, next) => {
//     try {
//         const body = req.body;
//         var updateDinner = await Dinner.findOneAndUpdate({ _id: body._id }, {
//             $set: { wait_time: body.wait_time }
//         })
//         if (updateDinner) {
//             return res.status(200).json("Updated Successfully!!!");
//         }
//         Dinner.findOne({ _id: body.id })
//             .exec()
//             .then(async DinnerList => {
//                 msg91.send(DinnerList.phone_number, ('Your Waiting Time is ' + body.wait_time + ' Minutes'), (err, response) => {
//                 });
//             });
//     } catch (err) {
//         let errors = err;
//         return res.status(500).json(errors);
//     }
// });

router.post("/default", (req, res, next) => {
  const newDefault = new default_time({
    default_time: 0
  });
  newDefault
    .save()
    .then(result => {
      res.status(201).json({
        message: "Default Time Added Successfully...!"
      });
    })
    .catch(err => {
      res.status(500).json({ error: err });
    });
});

//Update Default Wait Time
router.patch("/default_wait_time", async (req, res, next) => {
  try {
    const body = req.body;
    let updateDefaultWaitTime = await default_time.findOneAndUpdate(
      { _id: "5d41bea940aa480edade59b3" },
      {
        $set: {
          default_time: body.default_time ? body.default_time : 0
        }
      },
      { new: true }
    );
    if (updateDefaultWaitTime) {
      return res.status(201).json(updateDefaultWaitTime);
    }
  } catch (err) {
    return res.status(400).json("Error");
  }
});

router.post("/dinner_filter", (req, res, next) => {
  const body = req.body;
  Dinner.find({
    status: body.status,
    vendor_id: body.vendor_id
  })
    .exec()
    .then(DinnerList => {
      res.status(200).json(DinnerList);
    })
    .catch(err => {
      res.status(500).json({
        error: err
      });
    });
});

module.exports = router;
