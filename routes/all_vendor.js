const express = require("express");
const router = express.Router();
const Vendor = require("../models/vendor");

// Get To All the Vendor List Using /add_vendor route

router.get("/all_vendor", (req, res, next) => {
  Vendor.find()
    .exec()
    .then(docs => {
      //console.log(docs);
      res.status(200).json(docs);
    })
    .catch(err => {
      console.log(err);
      res.status(500).json({
        error: err
      });
    });
});

//   Get Vendor information by using /:status route(BYm status=)

router.get("/:status", (req, res, next) => {
  const status = req.params.status;
  Vendor.find({ status: status })
    .exec()
    .then(doc => {
      //console.log(doc);
      res.status(200).json(doc);
    })
    .catch(err => {
      console.log(err);
      res.status(500).json({ error: err });
    });
});

module.exports = router;
