import { DEFAULT_WAIT_TIME } from "./../types";
const initialState = {
  default_wait_time: 0
};

export default function(state = initialState, action) {
  switch (action.type) {
    case DEFAULT_WAIT_TIME:
      return {
        default_wait_time: action.payload
      };
    default:
      return state;
  }
}
