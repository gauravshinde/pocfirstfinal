import React from "react";
// import { Link } from "react-router-dom";

const VendorTableRow = ({
  no,
  vendorName,
  venderUser,
  vendorPassword,
  totalSeated,
  totalNonSeated,
  totalPeople,
  img,
  img1,
  alt,
  onClick,
  onClicktwo
}) => {
  return (
    <React.Fragment>
      <tr className="second-row">
        <td>{no}</td>
        <td className="text-capitalize">{vendorName}</td>
        <td>{venderUser}</td>
        {/* <td>{vendorPassword}</td> */}
        <td>{totalSeated}</td>
        <td>{totalNonSeated}</td>
        <td>{totalPeople}</td>
        <td>
          <img src={img1} alt={alt} onClick={onClick} />
        </td>
        <td>
          <img src={img} alt={alt} onClick={onClicktwo} />
        </td>
      </tr>
    </React.Fragment>
  );
};

export default VendorTableRow;
