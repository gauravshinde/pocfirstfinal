import React, { Component } from "react";
import PhoneInput from "react-phone-input-2";
import "react-phone-input-2/lib/style.css";
import InputComponent from "../SmallComponents/InputComponent";
import ButtonComponent from "../SmallComponents/ButtonComponent";
import LargeText from "../SmallComponents/LargeText";
import HeaderComponent from "../Header/HeaderComponent";
import TextBoxComponent from "../SmallComponents/TextBoxComponent";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Link, withRouter } from "react-router-dom";
import { registerDiner } from "../../store/actions/authAction";
import { addDinnerValidation } from "../../store/validation/addDinnerValidation";
import { logoutUser } from "../../store/actions/authAction";

export class VendorDiner extends Component {
  constructor() {
    super();
    this.state = {
      name: "",
      country_code: "",
      phone_number: "",
      adults: "",
      kids: "",
      special_occassion: "",
      errors: {}
    };
  }

  componentDidMount() {
    var defaultWaitTime = this.props.location.state.defaultWaitTime;
    this.setState({
      wait_time: defaultWaitTime
    });
  }
  componentWillReceiveProps(nextProps) {
    if (nextProps.errors) {
      this.setState({
        errors: nextProps.errors
      });
    }
  }

  handleAddDinerChange = e => {
    this.setState({
      [e.target.name]: e.target.value,
      error: {}
    });
  };

  handleAddDinerSubmit = e => {
    e.preventDefault();
    const { errors, isValid } = addDinnerValidation(this.state);
    if (!isValid) {
      this.setState({
        errors: errors
      });
    } else {
      const newDiner = {
        name: this.state.name,
        country_code: this.state.phone,
        phone_number: "+" + this.state.phone + this.state.phone_number,
        adults: this.state.adults,
        kids: this.state.kids,
        special_occassion: this.state.special_occassion,
        wait_time: this.state.wait_time,
        status: this.state.status
      };
      //console.log(newDiner);
      this.props.registerDiner(newDiner, this.props.history);
    }
  };

  onLogoutClick = e => {
    e.preventDefault();
    this.props.logoutUser(this.props.history);
  };

  render() {
    const { errors } = this.state;

    return (
      <React.Fragment>
        <HeaderComponent
          headertext={"Logo"}
          vendorDashboard={true}
          addVendorBtn={false}
          editVendorbtn={false}
          link={"/vendor-dashboard"}
          onClick={this.onLogoutClick}
        />
        <div className="container text-center">
          <div className="row">
            <div className="col-md-4" />

            <div className="col-md-4 add-vendor-align">
              <LargeText
                largetext={"Add Diner"}
                largetextclass={"addvendor-text"}
              />
              <hr />

              <form noValidate onSubmit={this.handleAddDinerSubmit}>
                <div className="d-flex justify-content-between">
                  <div className="form-group mb-0 text-left username-login-form-group w-50">
                    <label htmlFor="country_code">Country Code</label>
                    {/* <input
                      type="Number"
                      className="form-control input-bottomblack w-50 p-3"
                      name="country_code"
                      placeholder="91"
                      value={this.state.country_code}
                      onChange={this.handleAddDinerChange}
                    /> */}
                    <PhoneInput
                      country={""}
                      placeholder={"Select"}
                      value={this.state.phone}
                      onChange={phone => this.setState({ phone })}
                    />
                  </div>
                  <div>
                    <InputComponent
                      img={require("./../../assets/Images/phone-img.svg")}
                      alt={"Phone-img"}
                      labeltext={"Phone Number"}
                      name={"phone_number"}
                      type={"number"}
                      place={"eg. 123567890"}
                      inputclass={"form-control input-bottomblack "}
                      value={this.state.phone_number}
                      onChange={this.handleAddDinerChange}
                    />
                    <div className="col-12 main-validation-div">
                      {errors.phone_number ? (
                        <span className="isnotvalid">
                          {errors.phone_number}
                        </span>
                      ) : (
                        ""
                      )}
                    </div>
                  </div>
                </div>
                <InputComponent
                  img={require("./../../assets/Images/usernamepic.svg")}
                  alt={"user-img"}
                  labeltext={"Name"}
                  name={"name"}
                  type={"text"}
                  place={"eg. James Bond"}
                  inputclass={"form-control input-bottomblack "}
                  value={this.state.name}
                  onChange={this.handleAddDinerChange}
                  error={errors.name}
                />
                <div className="col-12 main-validation-div">
                  {errors.name ? (
                    <span className="isnotvalid">{errors.name}</span>
                  ) : (
                    ""
                  )}
                </div>
                <div className="form-row">
                  <div className="form-group col-xl-6">
                    <InputComponent
                      imageClass={"d-none"}
                      labeltext={"Adults"}
                      name={"adults"}
                      type={"number"}
                      place={"select"}
                      inputclass={"form-control pl-3 input-bottomblack"}
                      value={this.state.adults}
                      onChange={this.handleAddDinerChange}
                      error={errors.adults}
                    />
                  </div>

                  <div className="form-group col-xl-6">
                    <InputComponent
                      imageClass={"d-none"}
                      labeltext={"Kids"}
                      name={"kids"}
                      type={"number"}
                      place={"select"}
                      inputclass={"form-control pl-3 input-bottomblack"}
                      value={this.state.kids}
                      onChange={this.handleAddDinerChange}
                      error={errors.kids}
                    />
                  </div>
                </div>
                <TextBoxComponent
                  imageClass={"d-none"}
                  name={"special_occassion"}
                  type={"text"}
                  labeltext={"Special Ocassion"}
                  place={"eg. Birthday "}
                  Textareaclass={"form-control pl-3 textarea-bottomblack"}
                  value={this.state.special_occassion}
                  onChange={this.handleAddDinerChange}
                  error={errors.special_occassion}
                />
                <div className="col-12 main-validation-div mb-4">
                  {errors.special_occassion ? (
                    <span className="isnotvalid">
                      {errors.special_occassion}
                    </span>
                  ) : (
                    ""
                  )}
                </div>
                <ButtonComponent
                  buttontype={"submit"}
                  buttonclass={"login-button"}
                  buttontext={"Save"}
                  handleOnClick={this.nextClickHandler}
                />
                <Link to="/vendor-dashboard">
                  <ButtonComponent
                    buttontype={"button"}
                    buttonclass={"cancel-button"}
                    buttontext={"Cancel"}
                    handleOnClick={this.cancelClickHandler}
                  />
                </Link>
              </form>
            </div>
            <div className="col-md-4" />
          </div>
        </div>
      </React.Fragment>
    );
  }
}

VendorDiner.propTypes = {
  logoutUser: PropTypes.func.isRequired,
  registerDiner: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired,
  errors: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  auth: state.auth,
  errors: state.errors
});

export default connect(mapStateToProps, { logoutUser, registerDiner })(
  withRouter(VendorDiner)
);
