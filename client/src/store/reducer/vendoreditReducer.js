import { VENDOR_EDIT } from "../types";

const initialState = {
  vendoredit: []
};

export default function(state = initialState, action) {
  switch (action.type) {
    case VENDOR_EDIT:
      return {
        vendoredit: action.payload
      };
    default:
      return state;
  }
}
