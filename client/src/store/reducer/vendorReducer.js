import { VENDOR_LIST, CURRENT_LOGIN_VENDOR } from "../types";

const initialState = {
  vendor: [],
  loginVendor: []
};

export default function(state = initialState, action) {
  switch (action.type) {
    case VENDOR_LIST:
      return {
        vendor: action.payload
      };
    case CURRENT_LOGIN_VENDOR:
      return {
        loginVendor: action.payload
      };
    default:
      return state;
  }
}
