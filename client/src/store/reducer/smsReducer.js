import { SMS_SEND } from "../types";

const initialState = {
  smssend: []
};

export default function(state = initialState, action) {
  switch (action.type) {
    case SMS_SEND:
      return {
        smssend: action.payload
      };
    default:
      return state;
  }
}
