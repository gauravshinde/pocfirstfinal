import React, { Component } from "react";
// import ButtonComponent from "./ButtonComponent";
// import LargeText from "./LargeText";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
// import { sendWaitTimeSms } from "../../store/actions/smsSendAction";

export class OccassionPopup extends Component {
  constructor() {
    super();
    this.state = {
      notes: "",
      showPopUp: false
    };
  }

  static getDerivedStateFromProps(nextProps, nextState) {
    // console.log(nextProps.ordernotes.notes);
    if (nextProps.dinner.special_occassion !== nextState.notes) {
      return {
        notes: nextProps.dinner.special_occassion
      };
    }
  }

  onSubmit = e => {
    e.preventDefault();
    this.setState({
      showPopUp: false
    });
  };

  render() {
    const { notes } = this.state;
    if (this.state.showPopUp) {
      return (
        <React.Fragment>
          <div className="popup">
            <div className="popup_inner">
              {/* <LargeText largetext={"Send SMS"} largetextclass={"login-text"} /> */}
              <h3 className="popup-text">Special Occassion</h3>
              <div className="para-div">
                <p>{this.state.notes}</p>
              </div>

              <div className="order-notes-button">
                <button
                  type="submit"
                  className="login-button"
                  onClick={this.onSubmit}
                >
                  Okay
                </button>
              </div>
            </div>
          </div>
        </React.Fragment>
      );
    }
    return (
      <img
        src={require("./../../assets/Images/ocassion.svg")}
        alt="occassion"
        onClick={() => this.setState({ showPopUp: true })}
      />
      //   <h4
      //     className="ordernotes-text"
      //     onClick={() => this.setState({ showPopUp: true })}
      //   >
      //     Order Notes
      //   </h4>
    );
  }
}

const mapStateToProps = state => ({});

export default connect(mapStateToProps, {})(withRouter(OccassionPopup));

//sendWaitTimeSms
