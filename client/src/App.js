import React from "react";
import "./App.scss";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import Login from "./Components/Login/Login";
import VendorList from "./Components/VendorList/VendorList";
import VendorDetails from "./Components/VendorDetails/VendorDetails";
import AddVendor from "./Components/AddVendor/AddVendor";
import EditVendor from "./Components/EditVendor/EditVendor";
// import VendorLogin from "./Components/VendorLogin/VendorLogin";
import VendorDashboard from "./Components/VendorDashboard/VendorDashboard";
import VendorDiner from "./Components/VendorDiner/VendorDiner";
import setAuthToken from "./store/utils/setAuthToken";
import jwt_decode from "jwt-decode";
import { setCurrentUser } from "./store/actions/authAction";
import PrivateRoute from "./Components/private-route/PrivateRoute";

import { Provider } from "react-redux";
import store from "./store/store";

if (localStorage.getItem("jwtToken")) {
  // console.log(localStorage.jwtToken);
  setAuthToken(localStorage.jwtToken);
  const decoded = jwt_decode(localStorage.jwtToken);
  store.dispatch(setCurrentUser(decoded));
}

function App() {
  return (
    <Provider store={store}>
      <div className="App">
        <Router>
          <Switch>
            {/* main Route */}
            <Route exact path="/" component={Login} />
            {/* <Route exact path="/vendor-login" component={VendorLogin} /> */}

            {/* Private Route */}
            <PrivateRoute exact path="/vendor-list" component={VendorList} />
            <PrivateRoute
              exact
              path="/vendor-details"
              component={VendorDetails}
            />
            <PrivateRoute exact path="/add-vendor" component={AddVendor} />
            <PrivateRoute exact path="/edit-vendor" component={EditVendor} />
            <PrivateRoute
              exact
              path="/vendor-dashboard"
              component={VendorDashboard}
            />
            <PrivateRoute exact path="/add-diner" component={VendorDiner} />
          </Switch>
        </Router>
      </div>
    </Provider>
  );
}

export default App;
