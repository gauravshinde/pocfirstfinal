const express = require("express");
const mongoose = require("mongoose");
const bodyParser = require("body-parser");
const dinner = require("./routes/add_dinner");
const vendor = require("./routes/vendor");
const all_vendor = require("./routes/all_vendor");
const passport = require("passport");
const path = require("path");

const app = express();

// Body parser middleware
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

//DB CONFIG

const db = require("./config/keys").mongoURI;

// Connect to mogodb
mongoose
  .connect(db, { useNewUrlParser: true, useFindAndModify: true })
  .then(() => console.log("mongoDB Connected"))
  .catch(err => console.log(err));

//Passport Middleware
app.use(passport.initialize());

// passport Config
require("./config/passport")(passport);

// USE ROUTES
app.use("/all_vendor", all_vendor);
app.use("/vendor", vendor);
app.use("/dinner", dinner);

// SET STATIC FOLDER FOR PRODUCTTION BUILD
app.use(express.static("client/build"));
app.use("*", (req, res) => {
  res.sendFile(path.resolve(__dirname, "client", "build", "index.html"));
});

const port = process.env.PORT || 5000;

app.listen(port, () => console.log(`Server running on port ${port}`));
//data change
//20-12-2019
