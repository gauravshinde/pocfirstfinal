import axios from "axios";

import { STATUS_CHANGE } from "../types";

//get vendor data
export const getStatusChange = statusdata => dispatch => {
  console.log(statusdata);
  axios
    .patch(`/dinner/status`, statusdata)
    .then(res => console.log(res))
    .catch(err =>
      dispatch({
        type: STATUS_CHANGE,
        payload: err.response.data
      })
    );
};

export const set_new_waitTime = async formData => {
  console.log(formData, "asdas");
  let set_new_wait_time = await axios.patch("dinner/update_waitTime", formData);
  if (set_new_wait_time.data) {
    console.log("New Wait Time Set SuccessFulyy");
    window.location.reload();
  }
};

export const set_new_waitTime_store = formData => async dispatch => {
  console.log(formData, "asdas");
  let set_new_wait_time = await axios.patch("dinner/update_waitTime", formData);
  if (set_new_wait_time.data) {
    window.alert("New Wait Time Set SuccessFully!!!!");
    console.log("New Wait Time Set SuccessFulyy");
    window.location.reload();
  }
};

//.get("/dinner/all_dinner")
