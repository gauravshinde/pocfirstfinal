import axios from "axios";
import { DEFAULT_WAIT_TIME } from "./../types";

export const get_default_wait_time = () => async dispatch => {
  try {
    let getWaitTime = await axios.get("/dinner/get_wait_time");
    if (getWaitTime.data) {
      console.log(getWaitTime.data.default_time);
      dispatch({
        type: DEFAULT_WAIT_TIME,
        payload: getWaitTime.data.default_time
      });
    }
  } catch (err) {
    console.log(err);
  }
};

export const update_default_wait_time = formData => async dispatch => {
  try {
    let updateWaitTime = await axios.patch(
      "/dinner/default_wait_time",
      formData
    );
    if (updateWaitTime.data) {
      // dispatch( get_default_wait_time() )
      dispatch({
        type: DEFAULT_WAIT_TIME,
        payload: updateWaitTime.data.default_time
      });
    }
  } catch (err) {
    console.log(err);
  }
};
