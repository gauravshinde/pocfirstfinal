import axios from "axios";

import { VENDOR_LIST } from "../types";

/***************************************
 * @DESC - GET VENDOR DETAILS
 *****************************************/
export const getVendor = () => async dispatch => {
  try {
    let get_all_vendor = await axios.get("/vendor/all_vendor_list");
    if (get_all_vendor.data) {
      console.log(get_all_vendor.data);
      dispatch({
        type: VENDOR_LIST,
        payload: get_all_vendor.data
      });
    }
  } catch (err) {
    console.log(err);
  }
};
