const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const defaultSchema = new Schema({
    default_time: { type: Number, require: true }
});
module.exports = mongoose.model('default', defaultSchema);