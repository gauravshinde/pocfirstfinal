import React from "react";

const LargeText = ({
  largetext,
  largetextclass,
  count,
  formOne,
  addvendorform,
  mainclass
}) => {
  // console.log(formOne);
  return (
    <React.Fragment>
      <div className={mainclass}>
        {largetext ? <h4 className={largetextclass}>{largetext}</h4> : null}
        {addvendorform ? (
          count && formOne ? (
            <span>
              <b>1</b>/2
            </span>
          ) : (
            <span>
              <b>2</b>/2
            </span>
          )
        ) : (
          ""
        )}
      </div>
    </React.Fragment>
  );
};

export default LargeText;
